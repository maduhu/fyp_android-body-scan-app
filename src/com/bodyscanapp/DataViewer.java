package com.bodyscanapp;

import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class DataViewer extends ListActivity {
	
	
	  int count;
	  String[] selected_parts = new String[6];
	  int selected_parts_num = 0;
	  String[] selected_sensations = new String[20];
	  int sensation_num = 0;
	  int strength=-1;
	  int strength_op = -100;//-2 smaller, -1 smaller or equal,  0 equal, 1 equal or bigger, 2 bigger
	  String commans;
	  boolean use_command = false;
	  
	  public void onCreate(Bundle icicle) {
		  
		    super.onCreate(icicle);
		    
		    
		  // SharedPreferences config = this.getSharedPreferences("com.bodyscanapp",Context.MODE_PRIVATE);
		   commans = getIntent().getStringExtra("Command");
		   if(commans!=null && commans.length()>0) {
			   use_command = true;
			   setCommand(commans);
		   }
		   
		   final DataDB db = new DataDB(this);
		   count = db.getSize();
		 
		   String[] values = new String[count+1];
		   final int[] selections = new int[count+1];
		   values[0]="Add Selections";
		   
		   String entry="";
		   int select_id=1;
		   for(int i=count;i>0;i--){
			   JSONObject obj = db.get(i);
			   if(obj!=null){
				   try {
					   entry="";
					   String time= obj.getString("time");
					   String t=time.substring(0,4)+"/"+time.substring(4,6)+"/"+time.substring(6,8)+" "+time.substring(9,11)+":"+time.substring(11,13);
					   entry += "Time: "+t+"\n";
					   entry +="Body Part: "+obj.getString("part")+"\n";
					   entry +="Sensation: "+obj.getString("sensation")+"\n";
					   entry +="Strength: "+obj.getString("strength")+"\n";
					   if(use_command && check_match(obj.getString("part"), obj.getString("sensation"), obj.getString("strength"))){
						   
						   selections[select_id]=i;
						   values[select_id]=entry;
						   select_id += 1;
						   
					   }else if(!use_command){

						   values[count-i+1]=entry;   
					   }
					   
				   	} catch (JSONException e) {
					// TODO Auto-generated catch block
				   		e.printStackTrace();
				   	}
			   }
		   }
		   
		   if(use_command){
			   String[] nvalues = new String[select_id];
			   for(int i = 0;i<select_id;i++){
				   nvalues[i]=values[i];
			   }
			   values = nvalues;
		   }
	    
		    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
		    
		    this.setListAdapter(adapter);
		    
		    this.getListView().setOnItemClickListener(new OnItemClickListener(){
		    	
				@Override
				public void onItemClick(AdapterView<?> parent, View v, int position,
						long id) {
					// TODO Auto-generated method stub

					if(position == 0){
						//dialog for command
						final Dialog commandDialog=new Dialog(v.getContext());
						commandDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
						View layout1 = inflater.inflate(R.layout.selection_command, (ViewGroup)findViewById(R.id.command1));
						commandDialog.setContentView(layout1);
						
						final EditText command = (EditText) layout1.findViewById(R.id.command1);
						if(use_command) command.setText(getIntent().getStringExtra("Command"));
						Button setcommand = (Button) layout1.findViewById(R.id.button1);
						setcommand.setOnClickListener(new OnClickListener(){

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated method stub
								commandDialog.cancel();
								Intent i = new Intent(DataViewer.this, DataViewer.class);
								i.putExtra("Command", command.getText().toString());
								startActivity(i);
								finish();
							}
							
						});
						commandDialog.show();
					
						
					}else{
						
						final Dialog viewDataDialog=new Dialog(v.getContext());
						viewDataDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
						View layout = inflater.inflate(R.layout.date_viewer_popup, (ViewGroup)findViewById(R.id.dataviewerimage));
						viewDataDialog.setContentView(layout);
						
						Bitmap borigin=BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.body);
						Bitmap b = Bitmap.createBitmap(borigin.getWidth(),borigin.getHeight(),Bitmap.Config.ARGB_8888);
						
						Canvas c = new Canvas(b);
						Paint p = new Paint();
						//p.setStrokeWidth(10);
						p.setColor(Color.BLACK);
						c.drawBitmap(borigin, 0, 0,null);
						
						int at = 0;
						if(use_command){
							
							at = selections[position];
							
						}else{
							
							at = count-position+1;
						}
						JSONObject obj = db.get(at);
						
						
						try {
							
							//p.setStrokeWidth(20);
							p.setTextSize(30);
							c.drawText("Sensation: "+obj.getString("sensation"), 0, 550, p);
							c.drawText("Strength: "+obj.getString("strength"), 0, 580, p);
							c.drawText("Des: " +obj.getString("des"), 0, 610, p);
							
							if(obj.getInt("source")==1){
								//p.setStrokeWidth(6);
								//p.setStyle(Paint.Style.STROKE);
								double linepos = obj.getLong("linepos");
								float d = (float) (1.0*linepos);
								c.drawLine(0, d-10, 500, d-10, p);
								c.drawLine(0, d+10, 500, d+10, p);
							}else{
								float x =  obj.getInt("x");
								float y = obj.getInt("y");
	
								p.setStyle(Paint.Style.STROKE);
								c.drawCircle(x, y, 50, p);
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						ImageView iv = (ImageView) layout.findViewById(R.id.dataviewerimage);
						iv.setImageBitmap(b);
						viewDataDialog.show();
					}
				}
		    });
	  }
	  
	private boolean check_match(String part, String sensation, String xstrength) {
		// TODO Auto-generated method stub
		//
		boolean result = true;
		if(sensation_num>0){
			//do it this way is because the sensation maybe multiple 
			boolean c = false;
			for(int i =0; i<sensation_num;i++){
				if(sensation.contains(selected_sensations[i])){
					c=true;
					break;
				}
			}
			result = result&&c;
			//Toast.makeText(this, "Here 1", Toast.LENGTH_SHORT).show();
		}
		if(strength>=0&&strength_op>=-2){
			
			int s = 0;
			
			//if there is a list of strength value, use the max one.
			if(xstrength.contains("|")){
				String temp[] = xstrength.split("\\|");
				//Log.i("t0:", temp[0]+" "+temp[1]+" "+temp[2]);
				for(int i=0; i< temp.length;i++){
					//Log.i("t1:", temp[i]);
					String t = temp[i].replace(" ", "");
					//Log.i("t2:",t);
					t = t.replace("|", "");
					//Log.i("t3:",t);
					if(t.length()>0 && Integer.parseInt(t)>s)
						s=Integer.parseInt(t);
					//Log.i("t4:",s+"");
				}
			}else{
				s = Integer.parseInt(xstrength);
			}
			
			switch(strength_op){
				case -2: if(s>=strength) result = result&&false;
						break;
				case -1: if(s>strength) result = result&&false;
						break;
				case 0: if(s!=strength) result = result&&false; break;
				case 1: if(s<strength) result = result&&false; break;
				case 2: if(s<=strength) result = result&&false; break;
				default: result = result&&false;
			}
			//Toast.makeText(this, "Here 2", Toast.LENGTH_SHORT).show();
		}
		if(selected_parts_num>0 && !Arrays.asList(selected_parts).contains(part)){
			result = result&&false;
			//Toast.makeText(this, "Here 3", Toast.LENGTH_SHORT).show();
		}
		return result;
	}
	
	private void setCommand(String commands) {
		// TODO Auto-generated method stub
		String[] coms = commands.split(";");
		//Log.i("test set command", coms[0]+" length: "+coms.length);
		String[] parts={"head","body","left hand","right hand","left leg","right leg"};
		for(int i=0;i<coms.length;i++){
			if(coms[i].length()>0 && coms[i].contains("sensation")){
				
				String sensation = coms[i].replace("sensation=", "");
				selected_sensations[sensation_num] = sensation;
				sensation_num += 1;
				
			}else if(coms[i].length()>0 && coms[i].contains("strength")){
				
				String number = coms[i].replace("strength", "");
				number = number.replace(">", "");
				number = number.replace("<", "");
				number = number.replace("=", "");
				strength = Integer.parseInt(number);

				if(coms[i].contains("=")) strength_op = 0;
				if(coms[i].contains(">")) strength_op = 2;
				if(coms[i].contains("<")) strength_op = -2;
				if(coms[i].contains("<=")) strength_op = -1;
				if(coms[i].contains(">=")) strength_op = 1;
				
			}else if(coms[i].length()>0 &&Arrays.asList(parts).contains(coms[i])){
				//this checking method has serious bug. fix it if possible
				selected_parts[selected_parts_num] = coms[i];
				selected_parts_num ++;
			}
			
		}
	}
	  
	} 