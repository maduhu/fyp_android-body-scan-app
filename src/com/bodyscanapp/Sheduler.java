package com.bodyscanapp;

import org.json.JSONException;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;

public class Sheduler extends ListActivity {
	
	
	  //SharedPreferences config;
	  String[] shedules;
	  int count;
	  SheduleDB sdb;
	  public void onCreate(Bundle icicle) {
		  
		    super.onCreate(icicle);
		    
		    sdb=new SheduleDB(this);
		  // config = this.getSharedPreferences("com.bodyscanapp",Context.MODE_PRIVATE);
		  // count=config.getInt("shedulecount", 0);
		   
		   count=sdb.getSize();
		   
		   shedules=new String[count+1];

		   shedules[0]="Create New Schedule            +";
		   
		   
		   for(int i=1;i<=count;i++){

				   shedules[i]=sdb.getSheduleString(i);
		   }
	    
		    final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
		        android.R.layout.simple_list_item_1, shedules);
		    setListAdapter(adapter);
		    
		    getListView().setOnItemClickListener(new OnItemClickListener(){
		    	
				@Override
				public void onItemClick(AdapterView<?> parent, View v, final int position,
						long id) {
					// TODO Auto-generated method stub
					final PopupMenu pmenu=new PopupMenu(Sheduler.this,v);
					if(position==0){
						pmenu.getMenuInflater().inflate(R.menu.createshedulermenu, pmenu.getMenu());
						
						pmenu.setOnMenuItemClickListener(new OnMenuItemClickListener(){

							@Override
							public boolean onMenuItemClick(MenuItem i) {
								// TODO Auto-generated method stub
								Intent j;
								if(i.equals(pmenu.getMenu().getItem(0))){
									j=new Intent(Sheduler.this,FixTimeSheduler.class);
									j.putExtra("isEdit", false);
									startActivity(j);
									finish();
								}else{
									j=new Intent(Sheduler.this,RandomSheduler.class);
									j.putExtra("isEdit", false);
									startActivity(j);
									finish();
								}
								return false;
							}
							
						});
						
					}else
					{	
						pmenu.getMenuInflater().inflate(R.menu.clickshedule, pmenu.getMenu());
					
						pmenu.setOnMenuItemClickListener(new OnMenuItemClickListener(){

							@Override
							public boolean onMenuItemClick(MenuItem i) {
								// TODO Auto-generated method stub
								Intent j;
								if(i.equals(pmenu.getMenu().getItem(0))){
									int isFix=0;
									try {
										isFix=sdb.getShedule(position).getInt("isFix");
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									if(isFix==1)
										j=new Intent(Sheduler.this,FixTimeSheduler.class);
									else 
										j=new Intent(Sheduler.this,RandomSheduler.class);
									j.putExtra("isEdit", true);
									j.putExtra("At", position);
									startActivity(j);
									finish();
								}else{
									sdb.removeShedule(position);
									j=new Intent(Sheduler.this,Sheduler.class);
									startActivity(j);
									finish();
								}
								return false;
							}
						
						});
						
					}
					pmenu.show();
				}
		    });
	  }
	} 