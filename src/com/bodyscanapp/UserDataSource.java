package com.bodyscanapp;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class UserDataSource {
	
	//
	private SQLiteDatabase db;
	private MySQLiteHelper dbHelper;
	private String[] allColumns = {MySQLiteHelper.COLUMN_ID,MySQLiteHelper.COLUMN_DATE,
			MySQLiteHelper.SENSATION_TYPE};
	
	public UserDataSource(Context context){
		dbHelper = new MySQLiteHelper(context);
	}
	
	public void open() throws SQLException{
		db = dbHelper.getWritableDatabase();
	}
	
	public void close(){
		dbHelper.close();
	}
	
	public UserData createUserData(String date, String userData){
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_DATE, date);
		values.put(MySQLiteHelper.SENSATION_TYPE, userData);
		long insertId = db.insert(MySQLiteHelper.TABLE_USER_DATA, null, values);
		Cursor cursor = db.query(MySQLiteHelper.TABLE_USER_DATA, allColumns, MySQLiteHelper.COLUMN_ID+" = "+insertId, null, null, null, null);
		cursor.moveToFirst();
	    UserData newData = cursorToUserData(cursor);
	    cursor.close();
	    return newData;
	}
	
	private UserData cursorToUserData(Cursor cursor){
		UserData userdata = new UserData();
		userdata.setId(cursor.getLong(0));
		userdata.setDate(cursor.getString(1));
		userdata.setData(cursor.getString(2));
		return userdata;
	}
	
	public void deleteUserData(UserData userdata){
		long id = userdata.getId();
		System.out.println("UserData record deleted with id: "+ id);
		db.delete(MySQLiteHelper.TABLE_USER_DATA,MySQLiteHelper.COLUMN_ID+" = "+id,null);
	}
	
	public List<UserData> getAllUserData(){
		
		List<UserData> userdata = new ArrayList<UserData>();
		
		Cursor cursor = db.query(MySQLiteHelper.TABLE_USER_DATA, allColumns,null,null,null,null,null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()){
			UserData data = cursorToUserData(cursor);
			userdata.add(data);
			cursor.moveToNext();
		}
		
		cursor.close();
		return userdata;
	}
}
