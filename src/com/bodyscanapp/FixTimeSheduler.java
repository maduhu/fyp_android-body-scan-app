package com.bodyscanapp;

import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.TimePicker;

public class FixTimeSheduler extends Activity{
	
	
	boolean edit=false;
	boolean selectWeekDay[] = new boolean[7];
	int i=0;
	SheduleDB sdb;
	public void onCreate(Bundle icicle) {
	    super.onCreate(icicle);
		setContentView(R.layout.sheduler);
		
		edit = getIntent().getBooleanExtra("isEdit", false);
		sdb = new SheduleDB(this);
		
		final TimePicker tp=(TimePicker)findViewById(R.id.timePicker1);
		tp.setIs24HourView(true);
		//Timer now = new Timer();
		
		tp.setCurrentHour((Calendar.getInstance()).get(Calendar.HOUR_OF_DAY));
		tp.setCurrentMinute((Calendar.getInstance()).get(Calendar.MINUTE));
		
		final Button btns[] = new Button[9];
		btns[0]=(Button)findViewById(R.id.sheduler1);
		btns[1]=(Button)findViewById(R.id.sheduler2);
		btns[2]=(Button)findViewById(R.id.sheduler3);
		btns[3]=(Button)findViewById(R.id.sheduler4);
		btns[4]=(Button)findViewById(R.id.sheduler5);
		btns[5]=(Button)findViewById(R.id.sheduler6);
		btns[6]=(Button)findViewById(R.id.sheduler7);
		btns[7]=(Button)findViewById(R.id.shedulerbutton8);
		btns[8]=(Button)findViewById(R.id.shedulerbutton9);	
		int current_day_of_week=(Calendar.getInstance()).get(Calendar.DAY_OF_WEEK);
		btns[current_day_of_week-1].setTextColor(Color.RED);
		selectWeekDay[current_day_of_week-1]=true;
		
		
		final CheckBox cb = (CheckBox)findViewById(R.id.shedulercheckBox1);
		
		final RadioButton rbs[]=new RadioButton[2];
		rbs[0]=(RadioButton)findViewById(R.id.shedulerradioButton1);
		rbs[1]=(RadioButton)findViewById(R.id.shedulerradioButton2);
		//default option checked.	
		rbs[0].setChecked(true);
		
		for(i=0;i<7;i++){
			btns[i].setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					Button btn = (Button)v;
					for(int j=0;j<7;j++){
						if(btns[j].equals(btn)){
							if(btn.getTextColors().getDefaultColor()==Color.RED) {
								btn.setTextColor(Color.BLACK);
								selectWeekDay[j]=false;
							}
							else {
								btn.setTextColor(Color.RED);
								selectWeekDay[j]=true;
							}
						}
					}
				}	
			});
		}
		
		//done button. store the schedule
		btns[8].setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				boolean checked[] = new boolean[10];
				for(i=0;i<7;i++){
					checked[i]=selectWeekDay[i];
				}
				checked[7]=cb.isChecked();
				checked[8]=rbs[0].isChecked();
				checked[9]=rbs[1].isChecked();
				
				if(!edit)
					sdb.storeFixTimeShedule(tp.getCurrentHour(), tp.getCurrentMinute(), checked);
				else 
					sdb.editFixTimeShedule(getIntent().getIntExtra("At", 0),tp.getCurrentHour(), tp.getCurrentMinute(), checked);
				
				Intent i = new Intent(FixTimeSheduler.this,Sheduler.class);
				startActivity(i);
				finish();
			}
		});
		
		//cancel
		btns[7].setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				Intent i = new Intent(FixTimeSheduler.this,Sheduler.class);
				startActivity(i);
				finish();
			}
		});
		
		
		for(i=0;i<2;i++){
			rbs[i].setOnCheckedChangeListener(new OnCheckedChangeListener(){

				@Override
				public void onCheckedChanged(CompoundButton v, boolean b) {
					// TODO Auto-generated method stub
					if(rbs[0].equals(v)) rbs[1].setChecked(!b);
					else rbs[0].setChecked(!b);
				}
				
			});
		}
		
		
		if(edit){
			
			JSONObject myshedule=sdb.getShedule(getIntent().getIntExtra("At", 0));
			//Log.i("myshedule json string: ", myshedule.toString());
			try{
				tp.setCurrentHour(myshedule.getInt("hour"));
				tp.setCurrentMinute(myshedule.getInt("minute"));
				String[] checked= ((String)myshedule.get("checked")).split("#");
				for(i=0;i<7;i++){
					if(checked[i].compareToIgnoreCase("true")==0){
						selectWeekDay[i]=true;
						btns[i].setTextColor(Color.RED);
					}
				}

				cb.setChecked(checked[7].compareToIgnoreCase("true")==0);
				rbs[0].setChecked(checked[8].compareToIgnoreCase("true")==0);
				rbs[1].setChecked(checked[9].compareToIgnoreCase("true")==0);
				
			}catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
	}
	
}
