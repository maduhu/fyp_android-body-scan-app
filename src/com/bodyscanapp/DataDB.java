package com.bodyscanapp;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.format.Time;

public class DataDB {

	SharedPreferences db;
	String[] parts={"head","body","left hand","right hand","left leg","right leg"};
	public DataDB(Context context){
		db=context.getSharedPreferences("com.bodyscanapp_db",Context.MODE_PRIVATE);
	}
	public boolean store(float x, float y, int part,String sensation, String des, String strength){
		Time now = new Time();
    	now.setToNow();
    	
    	int count = db.getInt("count", 0);
    	count=count+1;
    	
    	JSONObject js= new JSONObject();
    	try {
			js.put("time", now.toString())
			  .put("x", x)
			  .put("y", y)
			  .put("part", parts[part])
			  .put("sensation", sensation)
			  .put("des", des)
			  .put("strength", strength)
			  .put("source", 0);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	db.edit().putInt("count", count)
    					 .putString("js"+count, js.toString())
    					 .commit();
		return true;
	}
	public boolean store(float linepos, int part, String sensation, String des, String strength){

    	Time now = new Time();
    	now.setToNow();
    	
    	int count = db.getInt("count", 0);
    	count=count+1;
    	
    	JSONObject js= new JSONObject();
    	try {
			js.put("time", now.toString())
			  .put("linepos", linepos)
			  .put("part", parts[part])
			  .put("sensation", sensation)
			  .put("des", des)
			  .put("strength", strength)
			  .put("source", 1);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	db.edit().putInt("count", count)
    					 .putString("js"+count, js.toString())
    					 .commit();
		return true;
	}
	public JSONObject get(int at){
		if(at>db.getInt("count", 0)||at<=0){
			return null;
		}
		JSONObject obj = null;
		try {
			obj = new JSONObject(db.getString("js"+at, ""));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}
	public int getSize(){
		return db.getInt("count", 0);
	}
}
