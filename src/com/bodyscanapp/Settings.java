package com.bodyscanapp;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

public class Settings extends ListActivity {
	
	
		int from=0;
		
	  public void onCreate(Bundle icicle) {
	    super.onCreate(icicle);
	    
	    if(getIntent().getStringExtra("From").compareToIgnoreCase("BodyScan")==0) from=0;
	    else if(getIntent().getStringExtra("From").compareToIgnoreCase("FullBodyScan")==0) from=1;
	    else from=-1;
	    
	   
	    String[] values = new String[] { 
	    		"Customize Scanning Options","Remove All Configuration Data","Remove All Scan Data", "Remove All Schedules", "Done"};
	    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
	        android.R.layout.simple_list_item_1, values);
	    setListAdapter(adapter);
	    
	    getListView().setOnItemClickListener(new OnItemClickListener(){

	    	Intent i;
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				switch(position){
				case 0:

					i = new Intent(Settings.this, ScanOptions.class);
					i.putExtra("From", getIntent().getStringExtra("From"));
					startActivity(i);
					finish();
					break;
				case 1:
					SharedPreferences config = getBaseContext().getSharedPreferences("com.bodyscanapp",Context.MODE_PRIVATE);
					config.edit().clear().commit();
					Toast.makeText(getBaseContext(), "All configuration data has been removed!", Toast.LENGTH_SHORT).show();
					break;
				case 2:
					SharedPreferences bodyscandb = getBaseContext().getSharedPreferences("com.bodyscanapp_db",Context.MODE_PRIVATE);
					bodyscandb.edit().clear().commit();
					Toast.makeText(getBaseContext(), "All scaning data has been removed!", Toast.LENGTH_SHORT).show();
					break;
				case 3:
					SheduleDB sdb = new SheduleDB(Settings.this);
					int size = sdb.getSize();
					for(int j=1;j<=size;j++)
						sdb.removeShedule(j);
					Toast.makeText(getBaseContext(), "All shedules has been removed!", Toast.LENGTH_SHORT).show();
					break;
				case 4:
					if(from == 0){
						i = new Intent(Settings.this,BodyScan.class);
						startActivity(i);
						finish();
					}else if(from == 1){
						i = new Intent(Settings.this,FullBodyScan.class);
						startActivity(i);
						finish();
					}else{
						Toast.makeText(getBaseContext(), "Error: Some unexpected error occured.", Toast.LENGTH_SHORT).show();
					}
					
					break;
				default:
					break;
				}
			}
	    	
	    });
	  }
	} 