package com.bodyscanapp;

import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TimePicker;
import android.widget.Toast;

public class RandomSheduler extends Activity{
	
	
	boolean edit=false;
	boolean selectWeekDay[] = new boolean[7];
	int i=0;
	SheduleDB sdb;
	int times[]=new int[6];
	boolean isRandom=true;
	
	public void onCreate(Bundle icicle) {
	    super.onCreate(icicle);
		setContentView(R.layout.randomsheduler);
		
		edit = getIntent().getBooleanExtra("isEdit", false);
		sdb = new SheduleDB(this);
		int temphour=(Calendar.getInstance()).get(Calendar.HOUR_OF_DAY);
		int tempminute=(Calendar.getInstance()).get(Calendar.MINUTE);
		times[0]=temphour;
		times[1]=tempminute;
		times[2]=temphour;
		times[3]=tempminute;
		//default is random
		times[4]=-1;
		times[5]=5;
		
		final Button btns[] = new Button[12];
		btns[0]=(Button)findViewById(R.id.random4);
		btns[1]=(Button)findViewById(R.id.random5);
		btns[2]=(Button)findViewById(R.id.random6);
		btns[3]=(Button)findViewById(R.id.random7);
		btns[4]=(Button)findViewById(R.id.random8);
		btns[5]=(Button)findViewById(R.id.random9);
		btns[6]=(Button)findViewById(R.id.random10);
		btns[7]=(Button)findViewById(R.id.random11);
		btns[8]=(Button)findViewById(R.id.random12);	
		btns[9]=(Button)findViewById(R.id.random1);
		btns[10]=(Button)findViewById(R.id.random2);
		btns[11]=(Button)findViewById(R.id.random3);
		
		
		final CheckBox cb = (CheckBox)findViewById(R.id.randomcheckBox1);
		
		final RadioButton rbs[]=new RadioButton[2];
		rbs[0]=(RadioButton)findViewById(R.id.randomradioButton1);
		rbs[1]=(RadioButton)findViewById(R.id.randomradioButton2);
		//default option checked.	
		rbs[0].setChecked(true);
		
		for(i=0;i<7;i++){
			btns[i].setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					Button btn = (Button)v;
					for(int j=0;j<7;j++){
						if(btns[j].equals(btn)){
							if(btn.getTextColors().getDefaultColor()==Color.RED) {
								btn.setTextColor(Color.BLACK);
								selectWeekDay[j]=false;
							}
							else {
								btn.setTextColor(Color.RED);
								selectWeekDay[j]=true;
							}
						}
					}
				}	
			});
		}
		
		//done button. store the schedule
		btns[8].setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				boolean checked[] = new boolean[10];
				for(i=0;i<7;i++){
					checked[i]=selectWeekDay[i];
				}
				checked[7]=cb.isChecked();
				checked[8]=rbs[0].isChecked();
				checked[9]=rbs[1].isChecked();
				
				if(!edit)
					sdb.storeRandomShedule(times, checked);
				else 
					sdb.editRandomShedule(getIntent().getIntExtra("At", 0),times, checked);
				
				Intent i = new Intent(RandomSheduler.this,Sheduler.class);
				startActivity(i);
				finish();
			}
		});
		
		//cancel
		btns[7].setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				Intent i = new Intent(RandomSheduler.this,Sheduler.class);
				startActivity(i);
				finish();
			}
		});
		
		btns[9].setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(final View v) {
				final Dialog startTimeDialog=new Dialog(v.getContext());
				startTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
				View layout = inflater.inflate(R.layout.timepickerdialog, (ViewGroup)findViewById(R.id.timePickerDialog));
				startTimeDialog.setContentView(layout);

				Button btn=(Button)layout.findViewById(R.id.button1);
				final TimePicker startTP=(TimePicker)layout.findViewById(R.id.timePickerDialog);
				startTP.setIs24HourView(true);
				
				startTP.setCurrentHour(times[0]);
				startTP.setCurrentMinute(times[1]);
				btn.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						times[0]=startTP.getCurrentHour();
						times[1]=startTP.getCurrentMinute();
						Button b =(Button)v;
						b.setText(times[0]+":"+times[1]);
						startTimeDialog.cancel();
					}});
				startTimeDialog.show();
			}
		});
		
		//listener for endtime button
		btns[10].setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(final View v) {
				final Dialog endTimeDialog=new Dialog(v.getContext());
				endTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
				View layout = inflater.inflate(R.layout.timepickerdialog, (ViewGroup)findViewById(R.id.timePickerDialog));
				endTimeDialog.setContentView(layout);
				
				final TimePicker endTP=(TimePicker)layout.findViewById(R.id.timePickerDialog);
				Button btn=(Button)layout.findViewById(R.id.button1);
				endTP.setIs24HourView(true);
				endTP.setCurrentHour(times[2]);
				endTP.setCurrentMinute(times[3]);
				btn.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						times[2]=endTP.getCurrentHour();
						times[3]=endTP.getCurrentMinute();
						Button b =(Button)v;
						b.setText(times[2]+":"+times[3]);
						endTimeDialog.cancel();
					}});
				endTimeDialog.show();
			}
		});
		
		//listener for interval button
		btns[11].setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(final View v) {
				final Dialog endTimeDialog=new Dialog(v.getContext());
				endTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
				View layout = inflater.inflate(R.layout.sheduleinterval, (ViewGroup)findViewById(R.id.radioButton1));
				endTimeDialog.setContentView(layout);
				
				final RadioButton rb1=(RadioButton)layout.findViewById(R.id.radioButton1);
				final RadioButton rb2=(RadioButton)layout.findViewById(R.id.radioButton2);
				final EditText et1 = (EditText)layout.findViewById(R.id.editText1);
				final EditText et2 = (EditText)layout.findViewById(R.id.editText2);
				Button btn=(Button)layout.findViewById(R.id.button1);
				if(times[4]<0) {
					rb1.setChecked(false);
					rb2.setChecked(true);
					et1.setEnabled(false);
					et2.setText(times[5]+"");
				}else{
					rb1.setChecked(true);
					rb2.setChecked(false);
					et1.setText(times[4]+"");
					et2.setEnabled(false);
				}
				rb1.setOnCheckedChangeListener(new OnCheckedChangeListener(){

					@Override
					public void onCheckedChanged(CompoundButton v,
							boolean checked) {
						if(checked) {
							et2.setEnabled(false);
							rb2.setChecked(false);
							et1.setEnabled(true);
						}
						else {
							et1.setEnabled(false);
							et2.setEnabled(true);
							rb2.setChecked(true);
						}
					}
					
				});
				rb2.setOnCheckedChangeListener(new OnCheckedChangeListener(){

					@Override
					public void onCheckedChanged(CompoundButton v,
							boolean checked) {
						if(checked) {
							et1.setEnabled(false);
							rb1.setChecked(false);
							et2.setEnabled(true);
						}
						else {
							et2.setEnabled(false);
							et1.setEnabled(true);
							rb1.setChecked(true);
						}
					}
					
				});
				
				btn.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Button b=(Button)v;
						if(rb2.isChecked()){
							times[4]=-1;
							int temp=Integer.parseInt(et2.getEditableText().toString());
							if(temp>0){
								times[5]=temp;
								b.setText("random "+temp);
								endTimeDialog.cancel();
							}else{
								Toast.makeText(arg0.getContext(), "scanning times must be number bigger than 0", Toast.LENGTH_SHORT).show();
							}
						}
						else{
							int t=Integer.parseInt(et1.getEditableText().toString());
							if(t>0){
								times[4]=t;
								b.setText("every "+times[4]+" min");
								endTimeDialog.cancel();
							}else{
								Toast.makeText(arg0.getContext(), "interval must be number bigger than 0", Toast.LENGTH_SHORT).show();
							}
						}
					}});
				
				endTimeDialog.show();
				
			}
		});
		
		for(i=0;i<2;i++){
			rbs[i].setOnCheckedChangeListener(new OnCheckedChangeListener(){

				@Override
				public void onCheckedChanged(CompoundButton v, boolean b) {
					// TODO Auto-generated method stub
					if(rbs[0].equals(v)) rbs[1].setChecked(!b);
					else rbs[0].setChecked(!b);
				}
				
			});
		}
		
		
		if(edit){
			
			JSONObject myshedule=sdb.getShedule(getIntent().getIntExtra("At", 0));
			//Log.i("myshedule json string: ", myshedule.toString());
			try{
				String[] checked= ((String)myshedule.get("checked")).split("#");
				for(i=0;i<7;i++){
					if(checked[i].compareToIgnoreCase("true")==0){
						selectWeekDay[i]=true;
						btns[i].setTextColor(Color.RED);
					}
				}

				cb.setChecked(checked[7].compareToIgnoreCase("true")==0);
				rbs[0].setChecked(checked[8].compareToIgnoreCase("true")==0);
				rbs[1].setChecked(checked[9].compareToIgnoreCase("true")==0);
				times[0]=myshedule.getInt("startHour");
				times[1]=myshedule.getInt("startMinute");
				times[2]=myshedule.getInt("endHour");
				times[3]=myshedule.getInt("endMinute");
				times[4]=myshedule.getInt("interval");
				times[5]=myshedule.getInt("scanningtimes");
				btns[9].setText(times[0]+":"+times[1]);
				btns[10].setText(times[2]+":"+times[3]);
				
			}catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			int current_day_of_week=(Calendar.getInstance()).get(Calendar.DAY_OF_WEEK);
			btns[current_day_of_week-1].setTextColor(Color.RED);
			selectWeekDay[current_day_of_week-1]=true;
		}
		
		
		
	}
	
}
