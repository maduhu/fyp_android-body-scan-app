package com.bodyscanapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

class AnimationView extends SurfaceView implements SurfaceHolder.Callback {
	
	Context context;

	int w,h;
	float speed=(float)0.5, StrokeWidth=2, linepos=0;
	int color;
	double hcount[]=new double[6];
	float wleft[]=new float[6];
	float wright[]=new float[6];
	int next=0;
    class AnimationThread extends Thread {

    	/** Are we running ? */
    	private boolean mRun;

        private SurfaceHolder mSurfaceHolder;

        /** How to display the text */
        private Paint paint;
        

        public AnimationThread(SurfaceHolder surfaceHolder) {
            mSurfaceHolder = surfaceHolder;

            /** Initiate the text painter */
            paint = new Paint();
            
        }

        /**
         * The actual game loop!
         */
        @Override
        public void run() {
        	super.run();
        	
        	paint.setColor(color);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(StrokeWidth);
            
            while (mRun) {
            	Canvas c = null;
                try {
                    c = mSurfaceHolder.lockCanvas(null);
                    synchronized (mSurfaceHolder) {

                        doDraw(c);
                    	updatePhysics();
                    }
                }finally {
                    if (c != null) {
                        mSurfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
        }

        /**
         * Figures the gamestate based on the passage of
         * realtime. Called at the start of draw().
         * Only calculates the FPS for now.
         */
        private void updatePhysics() {
        	if(linepos==0&&next!=0){
        		if(next==1||next==2||next==3) linepos=(float) hcount[0];
        		else linepos=(float) hcount[1];
        	}
            linepos += speed;
	        if(linepos>hcount[next]) {
	            	Intent intent = new Intent("BodyScan_Part_Finish_Notify");
	            	intent.putExtra("PartID", next);
	            	intent.putExtra("LinePos", linepos);
	            	context.sendBroadcast(intent);
	            	mRun=false;
	        }
        }

        /**
         * Draws to the provided Canvas.
         */
        private void doDraw(Canvas canvas) {

            // Draw the background color. Operations on the Canvas accumulate
            // so this is like clearing the screen. In a real game you can
        	if(canvas != null){
	        	// put in a background image of course
	        	//canvas.drawColor(Color.CYAN);
	        	Bitmap b = BitmapFactory.decodeResource(context.getResources(),R.drawable.body);
	        	//canvas.drawBitmap(b, 0, 0, null);
	        	
	        	canvas.drawBitmap(b, new Rect(0,0,b.getWidth(),b.getHeight()), new Rect(0,0,w,h), paint);
	        	canvas.drawLine(wleft[next], linepos, wright[next], linepos, paint);
	        	canvas.restore();
        	}
        }

        /**
         * So we can stop/pauze the game loop
         */
        public void setRunning(boolean b) {
            mRun = b;
        }      
        
        public float getLinePos(){
        	return linepos;
        }
        
        

    }

    /** The thread that actually draws the animation */
    private AnimationThread thread;

    public AnimationView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        // register our interest in hearing about changes to our surface
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);

        // create thread only; it's started in surfaceCreated()
        thread = new AnimationThread(holder);
    }

    /**
     * Obligatory method that belong to the:implements SurfaceHolder.Callback
     */

    /**
     * Callback invoked when the surface dimensions change.
     */
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    	this.w=width;
    	this.h=height;
    	hcount[0]=1.0*height/7;
    	hcount[1]=4.0*height/10;
    	hcount[2]=6.0*height/10;
    	hcount[3]=6.0*height/10;
    	hcount[4]=height;
    	hcount[5]=height;
    	wleft[0]=w/4;
    	wright[0]=6*w/11;
    	wleft[1]=w/5;
    	wright[1]=6*w/11;
    	wleft[2]=5;
    	wright[2]=w/4;
    	wleft[3]=6*w/11;
    	wright[3]=3*w/4;
    	wleft[4]=w/5;
    	wright[4]=2*w/5;
    	wleft[5]=2*w/5;
    	wright[5]=6*w/11;
    }

    /**
     * Callback invoked when the Surface has been created and is ready to be
     * used.
     */
    public void surfaceCreated(SurfaceHolder holder) {
        thread.setRunning(true);
    	thread.start();
    }

    /**
     * Callback invoked when the Surface has been destroyed and must no longer
     * be touched. WARNING: after this method returns, the Surface/Canvas must
     * never be touched again!
     */
    public void surfaceDestroyed(SurfaceHolder holder) {
        // we have to tell thread to shut down & wait for it to finish, or else
        // it might touch the Surface after we return and explode
        boolean retry = true;
        thread.setRunning(false);
        while (retry) {
            try {
            	//thread.destroy();
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
            	e.printStackTrace();
            }
        }
    }
    
    public float getPos(){
    	return thread.getLinePos();
    }
    
    public void surfacePause(){
    	thread.setRunning(false);
    }
    public void surfaceStart(float pos){
    	linepos=pos;
    	SurfaceHolder holder = getHolder();
        holder.addCallback(this);
    	thread = new AnimationThread(holder);
    	thread.setRunning(true);
    	thread.start();
    	
    }
    
    public void setAttr(float speed, int color, float StrokeWidth){
    	this.speed=speed;
    	this.color=color;
    	this.StrokeWidth=StrokeWidth;
    }
    
    public void setNext(int n){
    	if(n>=0&&n<=5) this.next=n;
    	else if(n>5){
    		next=0;
    		linepos=0;
    	}
    }

	public int getNext() {
		// TODO Auto-generated method stub
		return next;
	}
    
}