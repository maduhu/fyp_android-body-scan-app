package com.bodyscanapp;

import java.util.Timer;
import android.os.Bundle;
import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class FullBodyScan extends Activity {

	
	FrameLayout fl;
	Bitmap b;
	Button btn;
	ProgressBar pb;
	Timer t;
	int progress=0;
	
	float pos  = 0;
	
	String[] ImageSize= new String[] {
			"Image Size: Small",
			"Image Size: Medium",
			"Image Size: Large"
	};
	
	Boolean sensor_available=false;
	
	SharedPreferences config;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_body);
		
		
		
		//check external sensor plugged
		sensor_available = checkExternalSensor();
		config = this.getSharedPreferences("com.bodyscanapp",Context.MODE_PRIVATE);
		
		fl = (FrameLayout)findViewById(R.id.fView1);
		b = BitmapFactory.decodeResource(this.getResources(), R.drawable.body);
		RelativeLayout.LayoutParams rl = new RelativeLayout.LayoutParams(b.getWidth(),b.getHeight());
		rl.addRule(RelativeLayout.CENTER_HORIZONTAL);
		rl.addRule(RelativeLayout.BELOW,R.id.seekBarM1);
		fl.setLayoutParams(rl);
		
		ActionBar actionBar=getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		actionBar.setDisplayShowHomeEnabled(true);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_spinner_dropdown_item,ImageSize);
		
	
        actionBar.setListNavigationCallbacks(adapter, new OnNavigationListener() {
			 
            @Override
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        
            	float w,h;
            	w = (float) (1 + itemPosition*0.2);
				h = (float) (1 + itemPosition*0.2);
				RelativeLayout.LayoutParams rl = new RelativeLayout.LayoutParams(Math.round(b.getWidth()*w),Math.round(b.getHeight()*h));
				rl.addRule(RelativeLayout.CENTER_HORIZONTAL);
				rl.addRule(RelativeLayout.BELOW,R.id.seekBarM1);
				fl.setLayoutParams(rl);
            	
                return false;
            }
        });
        actionBar.setSelectedNavigationItem(0);
        
        
        TextView txss = (TextView) findViewById(R.id.textViewSS);
		txss.setVisibility(View.VISIBLE);
		
		SeekBar sb = (SeekBar) findViewById(R.id.seekBarM1);
		sb.setMax(10);
		sb.setVisibility(View.VISIBLE);
		
		final AnimationView av = new AnimationView(this, null);
		fl.addView(av);
		//retrieve and pass configuration attributes for scan line.
		config = this.getSharedPreferences("com.bodyscanapp",Context.MODE_PRIVATE);
		float speed = config.getFloat("ScanBarSpeed", (float)0.5);
		av.setAttr(config.getFloat("ScanBarSpeed", (float)0.5), 
				config.getInt("ScanBarColor", Color.RED),
				config.getFloat("ScanBarStrokeWidth", (float)2.0));
		
		
		//restore scan process from datarecording activity
		if(getIntent().getFloatExtra("Linepos", 0)!=0){
			av.surfacePause();
			av.setNext(getIntent().getIntExtra("Part", 0));
			av.surfaceStart(getIntent().getFloatExtra("Linepos", 0));
		}
		
		sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				
				config.edit().putFloat("ScanBarSpeed", (float)((seekBar.getProgress())*0.2)).commit();
				av.setAttr(config.getFloat("ScanBarSpeed", (float)0.5), 
						config.getInt("ScanBarColor", Color.RED),
						config.getFloat("ScanBarStrokeWidth", (float)2.0));
				
			}
		});
		sb.setProgress((int)(speed/0.2));

	
		//if sensor is not available, then create button and progress bar for user to indicate the strength
		if(!sensor_available){
	        
			btn = (Button) findViewById(R.id.bbm1);
			btn.setVisibility(View.VISIBLE);
			//pb = (ProgressBar) findViewById(R.id.pbm1);
			//pb.setVisibility(View.VISIBLE);
			
			btn.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					av.surfacePause();
					pos = av.getPos();
					
					final Dialog dataDialog=new Dialog(v.getContext());
					dataDialog.setTitle("Quick Record Sensation Data");
					LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
					View layout = inflater.inflate(R.layout.record_data_dialog, (ViewGroup)findViewById(R.id.dialog_data));
					dataDialog.setContentView(layout);
					
					Button diabtn1=(Button)layout.findViewById(R.id.button1);
					Button diabtn2=(Button)layout.findViewById(R.id.button2);
					Button diabtn3=(Button)layout.findViewById(R.id.button3);
					final SeekBar diasb=(SeekBar)layout.findViewById(R.id.dialog_seekbar);
					final Spinner diasp=(Spinner)layout.findViewById(R.id.spinner1);
					final EditText diatx=(EditText)layout.findViewById(R.id.editText1);
					diasb.setMax(100);
					
					String[] sensations= new String[] {
							"Select proper sensations",
							"Pain",
							"Tension",
							"Pressing",
							"Pulsation",
							"Numb",
							"Itchy",
							"Heat",
							"Cold",
							"Tickling",
							"Breathless",
							"Releasing",
							"Quick Vibration",
							"Floating"
					};
					ArrayAdapter<String> dia_adapter = 
							new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_spinner_dropdown_item,sensations);
					diasp.setAdapter(dia_adapter);
					
					diabtn1.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View v) {

							// TODO Auto-generated method stub
							String sensation=diasp.getItemAtPosition(diasp.getSelectedItemPosition()).toString();
							//Toast.makeText(v.getContext(), sensation, Toast.LENGTH_SHORT).show();
							String des=diatx.getText().toString();
							int strength=diasb.getProgress();
							recordData(pos,av.getNext(),sensation,des,strength);
							dataDialog.cancel();
							av.surfaceStart(pos);
						}
					});
					diabtn2.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dataDialog.cancel();
							Intent i =  new Intent(FullBodyScan.this, DataRecord.class);
							i.putExtra("Linepos", pos);
							i.putExtra("Part", av.getNext());
							i.putExtra("From", "FullBodyScan");
							i.putExtra("Sensation", diasp.getItemAtPosition(diasp.getSelectedItemPosition()).toString());
							i.putExtra("Strength", diasb.getProgress());
							i.putExtra("Des", diatx.getText().toString());
							startActivity(i);
						}
						
					});
					diabtn3.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dataDialog.cancel();
							av.surfaceStart(pos);
						}
						
					});
					dataDialog.show();
				}
				
			});
			
		}		
	}
	
	@Override
    public void onResume() {
            super.onResume();
            registerReceiver(broadcastReceiver, new IntentFilter(
                            "BodyScan_Part_Finish_Notify")); 
    }
	
	 @Override
     public void onPause() {
             super.onPause();
             unregisterReceiver(broadcastReceiver);
     }


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_body_scan, menu);
		menu.add(Menu.NONE,1,Menu.NONE,"Quick Recoding Feeling");
		menu.add(Menu.NONE,2,Menu.NONE,"View History Data");
		return true;
	}
	
	
	public boolean onOptionsItemSelected(MenuItem item){
		
		Intent i;
		switch(item.getItemId()){
			case 1:
				i= new Intent(FullBodyScan.this, BodyScan.class);
				startActivity(i);
				finish();
				break;
			case 2:
				i=new Intent(FullBodyScan.this,DataViewer.class);
				startActivity(i);
				break;
			default:
				i = new Intent(FullBodyScan.this,Settings.class);
				i.putExtra("From", "FullBodyScan");
				startActivity(i);
				finish();
				break;
		}
		return true;
	}
	
	private boolean checkExternalSensor(){
		return false;
	}
	
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
                final int PartID = intent.getIntExtra("PartID", -1);
                String[] parts={"head","body","left hand","right hand","left leg","right leg"};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
         
                final AnimationView av = (AnimationView)(fl.getChildAt(0));
                builder.setMessage("Scan "+parts[PartID]+" finished.\n"+"You can focus your mind on "
                		+parts[(PartID+1)%5]+" \nif you want to continue scanning exercise.")
                       .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int id) {
                        	  
                        	  if(PartID>=5){
                        		final Dialog overallExpDialog=new Dialog(context);
                        		overallExpDialog.setTitle("Overall Experience Feedback");
                  				LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                  				View layout = inflater.inflate(R.layout.overallexp, (ViewGroup)findViewById(R.id.textView1));
                  				overallExpDialog.setContentView(layout);
                  				Button btn1=(Button)layout.findViewById(R.id.button1);
                  				Button btn2=(Button)layout.findViewById(R.id.button2);
                  				btn2.setOnClickListener(new OnClickListener(){
									@Override
									public void onClick(View v) {
			                              overallExpDialog.cancel();
			                              av.setNext(PartID+1);
			                              av.surfaceStart(0);
									}
                  				});
                  				btn1.setOnClickListener(new OnClickListener(){
									@Override
									public void onClick(View v) {
										overallExpDialog.cancel();
										finish();
									}});
                  				overallExpDialog.show();
                        	  }else{
                        		  av.setNext(PartID+1);
	                              av.surfaceStart(0);
                        	  }
                           }
                       })
                       .setNegativeButton("Rescan", new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int id) {
                        	   av.setNext(PartID);
                               av.surfaceStart(0);
                           }
                       });
                
                builder.show();
        }
    };
    
    private void recordData(float linepos, int next, String sensation, String des, int strength){
    	DataDB db = new DataDB(this);
    	
    	if(db.store(linepos,next,sensation,des,""+strength))
    		Toast.makeText(getBaseContext(), "Scan Data Stored", Toast.LENGTH_SHORT).show();
    }
}
