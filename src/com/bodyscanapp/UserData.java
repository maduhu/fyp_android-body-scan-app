package com.bodyscanapp;

public class UserData {
	private long id;
	private String date;
	private String data;

	public long getId(){
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	public String getDate(){
		return date;
	}
	
	public void setDate(String d){
		this.date = d;
	}
	
	public String getData(){
		return data;
	}
	
	public void setData(String data){
		this.data = data;
	}
	
	public String dateToString() {
		return date;
	}
	
}
