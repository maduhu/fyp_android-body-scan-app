package com.bodyscanapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

	
	public static final String TABLE_USER_DATA = "comments";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_SENSSION_ID="senssion_id";
	public static final String COLUMN_COMPONENT_ID="component_id";
	public static final String COLUMN_DATE = "date";
	//use a json object to store all the data for one record
	public static final String SENSATION_TYPE = "sensation_type";
	public static final String COLUMN_START_TIME="start_time";
	public static final String COLUMN_END_TIME="end_time";
	public static final String COLUMN_STRENGTH="strength";
	
	private static final String DATABASE_NAME = "bodyscan.db";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "create table "
		      + TABLE_USER_DATA + "(" + COLUMN_ID
		      + " integer primary key autoincrement, " + COLUMN_SENSSION_ID
		      + " integer not null," + COLUMN_COMPONENT_ID
		      + "Integer not null,"+COLUMN_DATE
		      + "Integer "
		      + " text);";

	
	public MySQLiteHelper(Context context){
		super(context, DATABASE_NAME,null,DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		 Log.w(MySQLiteHelper.class.getName(),
			        "Upgrading database from version " + oldVersion + " to "
			            + newVersion + ", which will destroy all old data");
			    db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_DATA);
			    onCreate(db);
	}

}
