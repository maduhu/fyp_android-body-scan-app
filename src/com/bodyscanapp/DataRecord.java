package com.bodyscanapp;

import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Toast;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class DataRecord extends Activity {

	int comp;
	int pspeed=5;
	Timer t;
	int progress=0;
	Button b, br, be, ba;//first button pressed to record, second button click to remove the type
	SeekBar pb;
	RelativeLayout l;
	String userTypes;
	LayoutInflater myLI;
	LinearLayout myLL;
	ScrollView mySV;
	SharedPreferences config;
	//SharedPreferences config;
	//Editor e;
	
	int from=-1;
	String sensations="";
	String strengths="";
	String des="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.datainput);
		
		if(getIntent().getStringExtra("From").compareToIgnoreCase("BodyScan")==0) from=0;
	    else if(getIntent().getStringExtra("From").compareToIgnoreCase("FullBodyScan")==0) from=1;
	    else from=-1;
		
		sensations = getIntent().getStringExtra("Sensation");
		des = getIntent().getStringExtra("Des");
		//strengths+=getIntent().getIntExtra("Strength", 0);
		
		config =getBaseContext().getSharedPreferences("com.bodyscanapp_DataRecord",Context.MODE_PRIVATE);
		//e = config.edit();
		
		comp = getIntent().getIntExtra("ScanComponent", 1);
		
		myLI = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		myLL = new LinearLayout(this);
		myLL.setOrientation(LinearLayout.VERTICAL);
        myLL.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		mySV = (ScrollView)findViewById(R.id.scrollView1);
		((ViewGroup) mySV.getChildAt(0)).addView(myLL,0);
		
		ba = (Button) findViewById(R.id.buttonAddType);
		ba.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View view) {
				//pop up for new type name and position, 
				//default position at the end
				String name="";
				int pos = -1;
				inflateHelper(name,pos,0);
				popup(1,b,pos);
			}
        });
		
		
		final EditText desedit = (EditText)findViewById(R.id.editTextdr);
		if(des.compareToIgnoreCase("")!=0){
			desedit.setText(des);
		}
		
		Button done=(Button) findViewById(R.id.buttonDataDone);
		done.setOnClickListener(new Button.OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				DataDB db = new DataDB(getBaseContext());
				//SharedPreferences bodyscandb = getApplicationContext().getSharedPreferences("com.bodyscanapp_db",Context.MODE_PRIVATE);
				
				int count=config.getInt("Count", 0);
				for(int i=1;i<=count;i++){
					if((config.getInt("Delete"+i, 0))!=1){
						sensations+=" | "+config.getString("Sensation"+i, "unkown");
						strengths+=" | "+config.getInt("Strength"+i, 0);
					}
				}

				config.edit().clear().commit();
				
				if(from==0) {
					db.store(getIntent().getFloatExtra("x", 0), 
							getIntent().getFloatExtra("y", 0), 
							getIntent().getIntExtra("Part", 0),
							sensations,
							desedit.getText().toString(), 
							strengths);
					Intent i = new Intent(DataRecord.this,BodyScan.class);
					startActivity(i);
					finish();
				}else if(from ==1){
					db.store(getIntent().getFloatExtra("Linepos", 0),
							getIntent().getIntExtra("Part", 0), 
							sensations, 
							desedit.getText().toString(), 
							strengths);
					Intent i = new Intent(DataRecord.this,BodyScan.class);
					i.putExtra("Part", getIntent().getIntExtra("Part", 0));
					i.putExtra("Linepos", getIntent().getFloatExtra("Linepos", 0));
					startActivity(i);
					finish();
				}else{
					Toast.makeText(getBaseContext(), "Error: some error occurs", Toast.LENGTH_SHORT).show();
				}
			}
			
		});
		if(sensations.compareToIgnoreCase("Select proper sensations")!=0&&sensations.compareToIgnoreCase("")!=0){
			inflateHelper(sensations,0,getIntent().getIntExtra("Strength", 0));    
			config.edit().putInt("Count", 1)
						 .putString("Sensation"+1, sensations)
						 .putInt("Strength"+1, getIntent().getIntExtra("Strength", 0)).commit();
			sensations = "";
		}
		
	}
	
	
	//provide type name and position, helper will put a new type there
	private void inflateHelper(String typename, final int pos,int strength){
		//final SharedPreferences config =getBaseContext().getSharedPreferences("com.bodyscanapp_DataRecord",Context.MODE_PRIVATE);
		l = (RelativeLayout) myLI.inflate(R.layout.input_inflater, null);
		b = (Button) l.findViewById(R.id.ButtonInflate1);
		b.setText(typename);
		pb = (SeekBar)l.findViewById(R.id.SeekBarInflate1);
		pb.setProgress(strength);
		
		
		
		b.setOnTouchListener(new OnTouchListener(){
			public boolean onTouch(final View view, MotionEvent event){
				switch(event.getAction() & MotionEvent.ACTION_MASK){
				case MotionEvent.ACTION_DOWN:
					t= new Timer();
					t.schedule(new TimerTask(){
						public void run(){
							ProgressBar pbb;
							pbb = (ProgressBar)(((View) view.getParent()).findViewById(R.id.SeekBarInflate1));
							pbb.setProgress(progress);
							progress += 1;
						}
					}, 0, 1000/pspeed);
					break;
				case MotionEvent.ACTION_UP:
					t.cancel();
					progress = 0;
					break;
				default:
					break;
				}
				
				
				Button b = (Button)view;
				int realpos = 0;
				for(int i = 0; i<=config.getInt("Count", 0);i++){
					if(config.getString("Sensation"+i, "").compareToIgnoreCase(b.getText().toString())==0){
						realpos = i;
						break;
					}
				}
				config.edit().putString("Sensation"+realpos, b.getText().toString())
							 .putInt("Strength"+realpos, pb.getProgress())
							 .commit();
				return true;
			}
		});
		br = (Button) l.findViewById(R.id.buttonInflate2);
		br.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				Button b = (Button)((View) view.getParent()).findViewById(R.id.ButtonInflate1);
				int realpos = 0;
				for(int i = 0; i<=config.getInt("Count", 0);i++){
					if(config.getString("Sensation"+i, "").compareToIgnoreCase(b.getText().toString())==0){
						realpos = i;
						break;
					}
				}
				config.edit().putInt("Delete"+realpos, 1).commit();
				myLL.removeView((View) view.getParent());
			}
        });
		
		be = (Button) l.findViewById(R.id.buttonInflate3);
		be.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				//add a new dialog builder here
				//functions:
				//1. change type name
				//2. change progress bar speed
				//3. 
				Button b = (Button)((View) view.getParent()).findViewById(R.id.ButtonInflate1);
				int realpos = 0;
				for(int i = 0; i<=config.getInt("Count", 0);i++){
					if(config.getString("Sensation"+i, "").compareToIgnoreCase(b.getText().toString())==0){
						realpos = i;
						break;
					}
				}
				//Button b = (Button)((View) view.getParent()).findViewById(R.id.ButtonInflate1);
				popup(2,b,realpos);
			}
        });
		if(pos>=0)
			myLL.addView(l,pos);
		else
			myLL.addView(l);
	}
	
	private void popup(final int option,final Button b,final int pos){
		AlertDialog.Builder builder = new AlertDialog.Builder(DataRecord.this);
		
		//option 1, add type
		//option 2, edit type
		//final SharedPreferences config =getBaseContext().getSharedPreferences("com.bodyscanapp_DataRecord",Context.MODE_PRIVATE);
		
		if(option == 1){
			builder.setTitle("Add New Sensation");
		}else{
			builder.setTitle("Edit Sensation Name");
			//t.setText(name);
			//s.setProgress(speed);
		}
		final View v = myLI.inflate(R.layout.stedit, null);
		builder.setView(v);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				EditText t = (EditText) v.findViewById(R.id.editText1);
				//name = t.getText().toString();
				if(option==1){
					int count=config.getInt("Count", 0);
					config.edit().putInt("Count", count+1)
					 			 .putString("Sensation"+(count+1), t.getText().toString())
					 			 .commit();
					
				}else{
					config.edit().putString("Sensation"+pos, t.getText().toString())
								 .commit();
				}
				b.setText(t.getText().toString());
				
			}
		}).setNegativeButton("Cancel", null);
		builder.show();
	}

}
