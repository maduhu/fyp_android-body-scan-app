package com.bodyscanapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

public class MyAlarmReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context c, Intent i) {
		// TODO Auto-generated method stub
		Bundle b = i.getExtras();
		try {
			//Toast.makeText(c, "can i go here?", Toast.LENGTH_LONG).show();
			//Log.i("myalarmmanager", i.getStringExtra("com.bodyscanapp.SheduleDB.message"));
		     String message = b.getString("com.bodyscanapp.SheduleDB.message");
		     String event=b.getString("com.bodyscanapp.SheduleDB.event");
		     NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(c)
		     .setSmallIcon(R.drawable.icon)
		     .setContentTitle("BodyScan Schedule Notification")
		     .setContentText(message);
		     
		     Intent intent;
		     if(event.compareToIgnoreCase("BodyScan")==0){
		    	 intent = new Intent(c,BodyScan.class);
		     }else{
		    	 intent = new Intent(c,FullBodyScan.class);
		     }
		     PendingIntent mypi = PendingIntent.getActivity(c,0,intent, PendingIntent.FLAG_UPDATE_CURRENT);
		     mBuilder.setContentIntent(mypi);
		     NotificationManager mNoti = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
		     
		     Notification n = mBuilder.build();
		     n.flags |= Notification.FLAG_AUTO_CANCEL;
		     mNoti.notify(0,n);
		     
		     //Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
		    } catch (Exception e) {
		     //Toast.makeText(context, "There was an error somewhere, but we still received an alarm", Toast.LENGTH_SHORT).show();
		     e.printStackTrace();
		    }
	}
}