package com.bodyscanapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;

public class ScanOptions extends Activity {
	
	SharedPreferences config;
	Editor e;
	
	SeekBar sb1, sb2;
	Spinner sp;
	Button btn;
	
	int color;
	float speed, width;
	private String colors[];
	  public void onCreate(Bundle icicle) {
		  super.onCreate(icicle);
		  setContentView(R.layout.scan_options);
		  config = getSharedPreferences("com.bodyscanapp", Context.MODE_PRIVATE);
		  e = config.edit();
		  sp = (Spinner) findViewById(R.id.spinner1);
		  sb1 = (SeekBar) findViewById(R.id.seekBar1);
		  sb2 = (SeekBar) findViewById(R.id.seekBar2);
		  btn = (Button) findViewById(R.id.button1);
		  sb1.setMax(10);
		  sb2.setMax(10);
		  
		  float s= config.getFloat("ScanBarSpeed", (float)0.5);
		  float w = config.getFloat("ScanBarStrokeWidth", (float)2.0);
		  sb1.setProgress(Math.round(s*5));
		  sb2.setProgress((int) Math.round(w*2.5));
		  
		  colors = new String[]{"Red","Green","Blue","Black"};
		  ArrayAdapter<String> adapter0 = new ArrayAdapter<String>(ScanOptions.this, android.R.layout.simple_spinner_item, colors);
	      adapter0.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
	      sp.setAdapter(adapter0);
	      sp.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				switch(arg2){
				case 0: 
					color = Color.RED;
					break;
				case 1:
					color = Color.GREEN;
					break;
				case 2:
					color = Color.BLUE;
					break;
				case 3:
					color = Color.BLACK;
					break;
				default:
					break;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
	    	  
	      });
	      
	      btn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				speed = (float) (sb1.getProgress()*0.2);
				width = (float) (sb2.getProgress()*0.4);
				e.putFloat("ScanBarSpeed", speed);
				e.putFloat("ScanBarStrokeWidth", width);
				e.putInt("ScanBarColor", color);
				e.commit();
				Intent i = new Intent(ScanOptions.this, Settings.class);
				i.putExtra("From", getIntent().getStringExtra("From"));
				startActivity(i);
				finish();
			}
	    	  
	      });
	  }
}