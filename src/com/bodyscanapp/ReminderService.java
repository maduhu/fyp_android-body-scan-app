package com.bodyscanapp;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class ReminderService extends IntentService {
    private static final int NOTIF_ID = 1;

    public ReminderService(){
        super("ReminderService");
    }

    @Override
      protected void onHandleIntent(Intent intent) {
    	
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        
        String message = intent.getStringExtra("message");
   
       // long when = System.currentTimeMillis();         // notification time
        
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
	     .setSmallIcon(R.drawable.icon)
	     .setContentTitle("BodyScan Scheduling Notification")
	     .setContentText(message);
        
        Intent notificationIntent;
        String event=intent.getStringExtra("event");
        if(event.compareToIgnoreCase("BodyScan")==0){
	    	 notificationIntent = new Intent(this,BodyScan.class);
	     }else{
	    	 notificationIntent = new Intent(this,FullBodyScan.class);
	     }
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent , 0);
        mBuilder.setContentIntent(contentIntent);
        
        
        nm.notify(NOTIF_ID, mBuilder.build());
    }

}