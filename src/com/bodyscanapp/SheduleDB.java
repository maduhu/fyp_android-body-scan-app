package com.bodyscanapp;

import java.util.Calendar;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class SheduleDB {

	Context c;
	
	//use sqlite in future. preferences cannot handle too much data
	//here for convenience.
	SharedPreferences sheduleDB;
	public SheduleDB(Context context){
		c=context;
		sheduleDB=c.getSharedPreferences("com.bodyscanapp_sheduleDB",Context.MODE_PRIVATE);
	}
	
	public boolean storeFixTimeShedule(Integer hour, Integer minute, boolean[] checked){
		
		int id = getID();
		String cked="";
		for(int i=0;i<10;i++){
			if(checked[i]) cked+="true#";
			else cked+="false#";
		}
		String shedulestring="";
		if(checked[8]) shedulestring+="Quick Body Scan ";
		else shedulestring+="Full Body Scan ";
		shedulestring += "at "+hour+":"+minute+" \n";
		shedulestring +="on ";
		if(checked[0]) shedulestring+=" Sunday, ";
		if(checked[1]) shedulestring+=" Monday, ";
		if(checked[2]) shedulestring+=" Tuesday, ";
		if(checked[3]) shedulestring+=" Wednesday, ";
		if(checked[4]) shedulestring+=" Thurday, ";
		if(checked[5]) shedulestring+=" Friday, ";
		if(checked[6]) shedulestring+=" Saturday, ";

		if(checked[7]) shedulestring += "scheduled weekly repeat.";
		else shedulestring += "scheduled one time";
		
		
		JSONObject js = new JSONObject();
		try {
			js.put("hour", hour)
			  .put("minute", minute)
			  .put("checked", cked)
			  .put("sheduleID", id)
			  .put("sheduleString", shedulestring)
			  .put("isFix", 1);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int size = getSize();
		sheduleDB.edit().putInt("Size", size+1)
			            .putString("shedule"+(size+1), js.toString())
			            .commit();
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minute);
		

		AlarmManager am = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
		
		Intent i = new Intent(c,MyAlarmReceiver.class);

		i.putExtra("com.bodyscanapp.SheduleDB.message", shedulestring);
		if(checked[9]) {
			i.putExtra("com.bodyscanapp.SheduleDB.event", "FullBodyScan");
		}else {
			i.putExtra("com.bodyscanapp.SheduleDB.event", "BodyScan");
		}
		
		//PendingIntent pi = PendingIntent.getBroadcast(c, id, i, PendingIntent.FLAG_UPDATE_CURRENT);
		PendingIntent pis[]=new PendingIntent[7];
		int current_day_week=cal.get(Calendar.DAY_OF_WEEK);
		current_day_week-=1;
		if(checked[7]){
			for(int k=0;k<7;k++){
				if(checked[k]){
					pis[k]=PendingIntent.getBroadcast(c, id+1000*k, i, PendingIntent.FLAG_UPDATE_CURRENT);
					am.setRepeating(AlarmManager.RTC_WAKEUP, getMilisecLength(k,current_day_week)+cal.getTimeInMillis(), 1000*60*60*24*7, pis[k]);
				}
			}
		}
		else{
			for(int k=0;k<7;k++){
				if(checked[k]){
					pis[k]=PendingIntent.getBroadcast(c, id+1000*k, i, PendingIntent.FLAG_UPDATE_CURRENT);
					am.set(AlarmManager.RTC_WAKEUP, getMilisecLength(k,current_day_week)+cal.getTimeInMillis(), pis[k]);
				}
			}
		}
		//Toast.makeText(c, "value:"+cal.getTimeInMillis(), Toast.LENGTH_LONG).show();
		return true;
	}
	
	private long getMilisecLength(int k, int current_day_week) {
		// TODO Auto-generated method stub
		long miliDay=1000*60*60*24;
		//long miliDay=1000*30;
		if(current_day_week>k) return (k+7-current_day_week)*miliDay;
		else return (k-current_day_week)*miliDay;
	}

	
	public int getSize(){
		
		return sheduleDB.getInt("Size", 0);
	}
	
	public JSONObject getShedule(int at){
		
		if(at>getSize()) return null;
		
		String js=sheduleDB.getString("shedule"+at, null);
		
		JSONObject temp=null;
		try {
			temp = new JSONObject(js);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return temp;
	}
	
	public String getSheduleString(int at){

		if(at>getSize()) return null;
		
		String js=sheduleDB.getString("shedule"+at, null);
		
		JSONObject temp=null;
		String shedulestring="unknown";
		try {
			temp = new JSONObject(js);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			shedulestring = temp.getString("sheduleString");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return shedulestring;
	}
	
	public boolean removeShedule(int at){
		
		//remove shedule from alarmManager.
		cancelFromAlarmManager(at);
		
		int size = getSize();
		for(int j=1;j<size;j++){
			if(j>=at){
				sheduleDB.edit().putString("shedule"+j, sheduleDB.getString("shedule"+(j+1), null)).commit();
			}
		}
		sheduleDB.edit().remove("shedule"+size).commit();
		sheduleDB.edit().putInt("Size", size-1).commit();
		
		return true;
	}
	
	private int getID(){
		int id = sheduleDB.getInt("id", 0);
		sheduleDB.edit().putInt("id", id+1).commit();
		return id;
	}

	public boolean editFixTimeShedule(int at, int hour, int minute, boolean[] checked) {
		// TODO Auto-generated method stub
		cancelFromAlarmManager(at);
		removeShedule(at);
		storeFixTimeShedule(hour,minute,checked);
		return true;
		
	}
	public boolean cancelFromAlarmManager(int at){
		AlarmManager am = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
		int id = 0;
		String cked = "";
		boolean isFix=true;
		try {
			isFix=getShedule(at).getInt("isFix")==1;
			id = getShedule(at).getInt("sheduleID");
			cked=getShedule(at).getString("checked");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] checked=cked.split("#");
		
		Intent i = new Intent(c,MyAlarmReceiver.class);
		
		i.putExtra("com.bodyscanapp.SheduleDB.message", getSheduleString(at));
		
		if(checked[9].compareToIgnoreCase("true")==0) {
			i.putExtra("com.bodyscanapp.SheduleDB.event", "FullBodyScan");
		}else {
			i.putExtra("com.bodyscanapp.SheduleDB.event", "BodyScan");
		}
		
		if(isFix){
			PendingIntent pis[]=new PendingIntent[7];
			if(checked[7].compareToIgnoreCase("true")==0){
				for(int k=0;k<7;k++){
					if(checked[k].compareToIgnoreCase("true")==0){
						pis[k]=PendingIntent.getBroadcast(c, id+1000*k, i, PendingIntent.FLAG_UPDATE_CURRENT);
						am.cancel(pis[k]);
					}
				}
			}
			else{
				for(int k=0;k<7;k++){
					if(checked[k].compareToIgnoreCase("true")==0){
						pis[k]=PendingIntent.getBroadcast(c, id+1000*k, i, PendingIntent.FLAG_UPDATE_CURRENT);
						am.cancel(pis[k]);
					}
				}
			}
		}else{
			JSONObject js=getShedule(at);
			int times[]=new int[6];
			try {
				times[0]=js.getInt("startHour");
				times[1]=js.getInt("startMinute");
				times[2]=js.getInt("endHour");
				times[3]=js.getInt("endMinute");
				times[4]=js.getInt("interval");
				times[5]=js.getInt("scanningtimes");
				} catch (JSONException e){
				// TODO Auto-generated catch block
			
					e.printStackTrace();
				}
			int totalMinute=(times[2]-times[0])*60+(times[3]-times[1]);
			int piscount=0;
			boolean isRandom=times[4]<0;
			if(isRandom) piscount=times[5];
			else piscount=totalMinute/times[4];
			PendingIntent pis[][]=new PendingIntent[7][piscount+1];
			if(checked[7].compareToIgnoreCase("true")==0){
				for(int k=0;k<7;k++){
					if(checked[k].compareToIgnoreCase("true")==0){
						//if randomly sheduled
						if(isRandom){
							for(int j=0;j<times[5];j++){
								pis[k][j]=PendingIntent.getBroadcast(c, id+1000*k+10000*j, i, PendingIntent.FLAG_UPDATE_CURRENT);
								
								am.cancel(pis[k][j]);
							}
						}else{
							//not random case
							for(int j=0;j<totalMinute;j+=times[5]){
								pis[k][j]=PendingIntent.getBroadcast(c, id+1000*k+10000*(j/times[5]), i, PendingIntent.FLAG_UPDATE_CURRENT);
								//wait for r minutes to trigger 
								am.cancel(pis[k][j]);
							}
						}
					}
				}
			}
			else{
				for(int k=0;k<7;k++){
					if(checked[k].compareToIgnoreCase("true")==0){
						if(isRandom){
							
							for(int j=0;j<times[5];j++){
								pis[k][j]=PendingIntent.getBroadcast(c, id+1000*k+10000*j, i, PendingIntent.FLAG_UPDATE_CURRENT);
								am.cancel(pis[k][j]);
							}
						}else{
							for(int j=0;j<totalMinute;j+=times[5]){
								pis[k][j]=PendingIntent.getBroadcast(c, id+1000*k+10000*(j/times[5]), i, PendingIntent.FLAG_UPDATE_CURRENT);
								am.cancel(pis[k][j]);
							}
						}
					}
				}
			}
		}
		
		return true;
	}

	public void storeRandomShedule(int[] times, boolean[] checked) {
		// TODO Auto-generated method stub

		boolean isRandom=times[4]<0;
		int id = getID();
		String cked="";
		for(int i=0;i<10;i++){
			if(checked[i]) cked+="true#";
			else cked+="false#";
		}
		String shedulestring="";
		if(checked[8]) shedulestring+="Quick Body Scan ";
		else shedulestring+="Full Body Scan ";
		if(isRandom) shedulestring+=times[5]+" times randomly\n";
		else shedulestring+="every "+times[4]+" munites\n";
		shedulestring+="from "+times[0]+":"+times[1]+" to "+times[2]+":"+times[3];
		shedulestring +="\non ";
		if(checked[0]) shedulestring+=" Sunday, ";
		if(checked[1]) shedulestring+=" Monday, ";
		if(checked[2]) shedulestring+=" Tuesday, ";
		if(checked[3]) shedulestring+=" Wednesday, ";
		if(checked[4]) shedulestring+=" Thurday, ";
		if(checked[5]) shedulestring+=" Friday, ";
		if(checked[6]) shedulestring+=" Saturday, ";

		if(checked[7]) shedulestring += "sheduled weekly repeat.";
		else shedulestring += "sheduled one time";
		
		
		JSONObject js = new JSONObject();
		try {
			js.put("startHour", times[0])
			  .put("startMinute", times[1])
			  .put("endHour", times[2])
			  .put("endMinute", times[3])
			  .put("interval", times[4])
			  .put("scanningtimes",times[5])
			  .put("checked", cked)
			  .put("sheduleID", id)
			  .put("sheduleString", shedulestring)
			  .put("isFix", 0);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int size = getSize();
		sheduleDB.edit().putInt("Size", size+1)
			            .putString("shedule"+(size+1), js.toString())
			            .commit();
		
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, times[0]);
		cal.set(Calendar.MINUTE, times[1]);

		AlarmManager am = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
		
		Intent i = new Intent(c,MyAlarmReceiver.class);

		i.putExtra("com.bodyscanapp.SheduleDB.message", shedulestring);
		if(checked[9]) {
			i.putExtra("com.bodyscanapp.SheduleDB.event", "FullBodyScan");
		}else {
			i.putExtra("com.bodyscanapp.SheduleDB.event", "BodyScan");
		}
		
		//PendingIntent pi = PendingIntent.getBroadcast(c, id, i, PendingIntent.FLAG_UPDATE_CURRENT);
		int totalMinute=(times[2]-times[0])*60+(times[3]-times[1]);
		int piscount=0;
		if(isRandom) piscount=times[5];
		else piscount=totalMinute/times[4];
		PendingIntent pis[][]=new PendingIntent[7][piscount+1];
		
		int current_day_week=cal.get(Calendar.DAY_OF_WEEK);
		current_day_week-=1;
		if(checked[7]){
			for(int k=0;k<7;k++){
				if(checked[k]){
					//if randomly sheduled
					if(isRandom){
						
						Random rand= new Random(0);
					
						for(int j=0;j<times[5];j++){
							int r=0;
							if(totalMinute>0)
								r=rand.nextInt(totalMinute);
							
							//let id=id+1000*k+10000*j to ensure every alarm has a unique id
							pis[k][j]=PendingIntent.getBroadcast(c, id+1000*k+10000*j, i, PendingIntent.FLAG_UPDATE_CURRENT);
							//wait for r minutes to trigger 
							am.setRepeating(AlarmManager.RTC_WAKEUP, getMilisecLength(k,current_day_week)+cal.getTimeInMillis()+r*60*1000, 1000*60*60*24*7, pis[k][j]);
						}
					}else{
						//not random case
						//temp is total minutes from start time to end time
						for(int j=0;j<totalMinute;j+=times[4]){//increase every interval minutes
							//let id=id+1000*k+10000*j to ensure every alarm has a unique id
							pis[k][j/times[4]]=PendingIntent.getBroadcast(c, id+1000*k+10000*(j/times[4]), i, PendingIntent.FLAG_UPDATE_CURRENT);
							//wait for r minutes to trigger 
							am.setRepeating(AlarmManager.RTC_WAKEUP, getMilisecLength(k,current_day_week)+cal.getTimeInMillis()+j*60*1000, 1000*60*60*24*7, pis[k][j/times[4]]);
						}
					}
				}
			}
		}
		else{
			for(int k=0;k<7;k++){
				if(checked[k]){
					if(isRandom){
						
						Random rand= new Random(0);
					
						for(int j=0;j<times[5];j++){
							int temp = (times[2]-times[0])*60+(times[3]-times[1]);
							int r=0;
							if(temp>0)
								r=rand.nextInt((times[2]-times[0])*60+(times[3]-times[1]));
							
							//let id=id+1000*k+10000*j to ensure every alarm has a unique id
							pis[k][j]=PendingIntent.getBroadcast(c, id+1000*k+10000*j, i, PendingIntent.FLAG_UPDATE_CURRENT);
							//wait for r minutes to trigger 
							am.set(AlarmManager.RTC_WAKEUP, getMilisecLength(k,current_day_week)+cal.getTimeInMillis()+r*60*1000,  pis[k][j]);
						}
					}else{
						//not random case
						for(int j=0;j<totalMinute;j+=times[4]){//increase every interval minutes
							//let id=id+1000*k+10000*j to ensure every alarm has a unique id
							pis[k][j/times[4]]=PendingIntent.getBroadcast(c, id+1000*k+10000*(j/times[4]), i, PendingIntent.FLAG_UPDATE_CURRENT);
							//wait for r minutes to trigger 
							am.set(AlarmManager.RTC_WAKEUP, getMilisecLength(k,current_day_week)+cal.getTimeInMillis()+j*60*1000, pis[k][j/times[4]]);
						}
					}
				}
			}
		}
	}
	
	public void editRandomShedule(int at, int[] times, boolean[] checked) {
		// TODO Auto-generated method stub
		cancelFromAlarmManager(at);
		removeShedule(at);
		storeRandomShedule(times,checked);
	}
}
