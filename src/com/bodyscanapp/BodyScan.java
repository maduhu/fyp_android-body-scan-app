package com.bodyscanapp;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.ActionBar.OnNavigationListener;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class BodyScan extends Activity {

	
	FrameLayout fl;
	Bitmap b;
	Button btn;
	ProgressBar pb;
	float sizeRatio=1;
	float w,h;
	String[] ImageSize= new String[] {
			"Image Size: Small",
			"Image Size: Medium",
			"Image Size: Large"
	};
	float x,y;
	Boolean sensor_available=false;
	
	//SharedPreferences config;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_body);
		
		
		
		//config = this.getSharedPreferences("com.bodyscanapp",Context.MODE_PRIVATE);
		
		fl = (FrameLayout)findViewById(R.id.fView1);
		b = BitmapFactory.decodeResource(this.getResources(), R.drawable.body);


		
		RelativeLayout.LayoutParams rl = new RelativeLayout.LayoutParams(b.getWidth(),b.getHeight());
		rl.addRule(RelativeLayout.CENTER_HORIZONTAL);
		rl.addRule(RelativeLayout.BELOW,R.id.seekBarM1);
		fl.setLayoutParams(rl);
		
		//quick mode
		ImageView iv = new ImageView(this);
		iv.setImageBitmap(b);
		w=b.getWidth();
		h=b.getHeight();
		iv.setOnTouchListener(new OnTouchListener(){

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				x=event.getX();
				y=event.getY();
				return false;
			}
			
		});
		iv.setOnLongClickListener(new OnLongClickListener(){

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				
				final Dialog dataDialog=new Dialog(v.getContext());
				dataDialog.setTitle("Quick Record Sensation Data");
				LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
				View layout = inflater.inflate(R.layout.record_data_dialog, (ViewGroup)findViewById(R.id.dialog_data));
				dataDialog.setContentView(layout);
				
				Button diabtn1=(Button)layout.findViewById(R.id.button1);
				Button diabtn2=(Button)layout.findViewById(R.id.button2);
				Button diabtn3=(Button)layout.findViewById(R.id.button3);
				final SeekBar diasb=(SeekBar)layout.findViewById(R.id.dialog_seekbar);
				final Spinner diasp=(Spinner)layout.findViewById(R.id.spinner1);
				final EditText diatx=(EditText)layout.findViewById(R.id.editText1);
				diasb.setMax(100);
				
				String[] sensations= new String[] {
						"Select proper sensations",
						"Pain",
						"Tension",
						"Pressing",
						"Pulsation",
						"Numb",
						"Itchy",
						"Heat",
						"Cold",
						"Tickling",
						"Breathless",
						"Releasing",
						"Quick Vibration",
						"Floating"
				};
				ArrayAdapter<String> dia_adapter = 
						new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_spinner_dropdown_item,sensations);
				diasp.setAdapter(dia_adapter);
				
				diabtn1.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {

						// TODO Auto-generated method stub
						String sensation=diasp.getItemAtPosition(diasp.getSelectedItemPosition()).toString();
						//Toast.makeText(v.getContext(), sensation, Toast.LENGTH_SHORT).show();
						String des=diatx.getText().toString();
						int strength=diasb.getProgress();
						
						recordData(x,y,sizeRatio,sensation,des,strength);
						dataDialog.cancel();
					}

				
				});
				diabtn2.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dataDialog.cancel();
						Intent i =  new Intent(BodyScan.this, DataRecord.class);
						i.putExtra("From", "BodyScan");
						i.putExtra("Part", partName(x,y,sizeRatio));
						i.putExtra("x", x/sizeRatio);
						i.putExtra("y", y/sizeRatio);
						i.putExtra("Sensation", diasp.getItemAtPosition(diasp.getSelectedItemPosition()).toString());
						i.putExtra("Strength", diasb.getProgress());
						i.putExtra("Des", diatx.getText().toString());
						//Log.i("Strength", "strength is "+diasb.getProgress());
						//Toast.makeText(getBaseContext(), diasb.getProgress(), Toast.LENGTH_SHORT).show();
						startActivity(i);
					}
					
				});
				diabtn3.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dataDialog.cancel();
					}
					
				});
				dataDialog.show();
				return true;
			}
		});
		fl.addView(iv);
		
		SeekBar sb = (SeekBar) findViewById(R.id.seekBarM1);
		sb.setVisibility(View.INVISIBLE);
		TextView txss = (TextView) findViewById(R.id.textViewSS);
		txss.setVisibility(View.INVISIBLE);
		
		ActionBar actionBar=getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		actionBar.setDisplayShowHomeEnabled(true);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_spinner_dropdown_item,ImageSize);
		
	
        actionBar.setListNavigationCallbacks(adapter, new OnNavigationListener() {
			 
            @Override
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        
            	float w,h;
            	w = (float) (1 + itemPosition*0.2);
				h = (float) (1 + itemPosition*0.2);
				RelativeLayout.LayoutParams rl = new RelativeLayout.LayoutParams(Math.round(b.getWidth()*w),Math.round(b.getHeight()*h));
				rl.addRule(RelativeLayout.CENTER_HORIZONTAL);
				rl.addRule(RelativeLayout.BELOW,R.id.seekBarM1);
				fl.setLayoutParams(rl);
            	sizeRatio=w;
                return false;
            }
        });
        actionBar.setSelectedNavigationItem(0);
	}
	
	@Override
    public void onResume() {
            super.onResume();
    }
	
	 @Override
     public void onPause() {
             super.onPause();
     }


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_body_scan, menu);
		menu.add(Menu.NONE,1,Menu.NONE,"Full Scan");
		menu.add(Menu.NONE,2,Menu.NONE,"View History Record");
		menu.add(Menu.NONE,3,Menu.NONE,"Schedule Scanning");
		return true;
	}
	
	
	public boolean onOptionsItemSelected(MenuItem item){
		
		Intent i;
		switch(item.getItemId()){
		case 1:
			i = new Intent(BodyScan.this,FullBodyScan.class);
			startActivity(i);
			finish();
			break;
		case 2:
			i = new Intent(BodyScan.this,DataViewer.class);
			startActivity(i);
			break;
		case 3:
			i = new Intent(BodyScan.this,Sheduler.class);
			startActivity(i);
			break;
		default:
			i = new Intent(BodyScan.this,Settings.class);
			i.putExtra("From", "BodyScan");
			startActivity(i);
			finish();
			break;
		}
		return true;
	}
	
	private void recordData(float x, float y, float ratio,String sensation,
			String des, int strength) {
		// TODO Auto-generated method stub
		int part = partName(x,y,ratio);
		DataDB db = new DataDB(this);
		db.store(x/ratio, y/ratio, part, sensation, des, ""+strength);
		Toast.makeText(getBaseContext(), "Scan Data Stored", Toast.LENGTH_SHORT).show();
	}
	
	//given coordinate x,y of the body image, together with the image size ratio, to calculate the 
	//corresponding part.
	private int partName(float x, float y, float sizeratio){
		float nx=(float) (1.0*x/sizeratio);
		float ny=(float) (1.0*y/sizeratio);
		Log.i("ny", "x: "+x+"ny: "+ny+" h/7: "+h/7);
		if(ny<(h/7)) return 0;
		else if(ny<(6*h/10)&&nx<(w/4)) return 2;
		else if(ny<(6*h/10)&&nx>(6*w/11)) return 3;
		else if(ny<(4*h/10)) return 1;
		else if(nx<(2*w/5)) return 4;
		else return 5;
	}
}
